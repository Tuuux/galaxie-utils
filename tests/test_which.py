import unittest
import os
from GLXUtils.which import which


class TestWhich(unittest.TestCase):
    def test_which(self):
        """Test utils.which()"""
        # Test Raises
        self.assertRaises(TypeError, which, program_name=None)
        self.assertRaises(EnvironmentError, which, program_name="lulu4224")
        # do the job
        self.assertTrue(
            isinstance(which(program_name="python", all=False), str)
        )
        self.assertEqual(
            os.path.basename(which(program_name="python")), "python"
        )
        self.assertTrue(
            isinstance(which(program_name="python", all=True), list)
        )
        for element in which(program_name="python", all=True):
            self.assertEqual(os.path.basename(element), "python")


if __name__ == "__main__":
    unittest.main()
