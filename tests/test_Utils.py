#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from numpy import isneginf

from GLXUtils.utils import Utils


class TestUtils(unittest.TestCase):

    def test_round_up(self):
        with Utils() as utils:
            self.assertRaises(
                TypeError, utils.round_up)
            self.assertRaises(
                TypeError, utils.round_up, number=None, decimals=0)
            self.assertRaises(
                TypeError, utils.round_up, number=42.65, decimals=None)
            self.assertEquals(12, utils.round_up(number=11.1))
            self.assertEquals(11, utils.round_up(11.0))
            self.assertEquals(11, utils.round_up(10.9))
            self.assertEquals(11, utils.round_up(10.1))
            self.assertEquals(10, utils.round_up(10.0))
    
    def test_round_down(self):
        with Utils() as utils:
            self.assertRaises(
                TypeError, utils.round_down)
            self.assertRaises(
                TypeError, utils.round_down, number=None, decimals=0)
            self.assertRaises(
                TypeError, utils.round_down, number=42.65, decimals='None')
            self.assertRaises(
                TypeError, utils.round_down, number=42.65, decimals=43.2)
            self.assertEquals(11, utils.round_down(number=11.1))
            self.assertEquals(11, utils.round_down(number=11.0))
            self.assertEquals(10, utils.round_down(number=10.9))
            self.assertEquals(10, utils.round_down(number=10.1))
            self.assertEquals(10, utils.round_down(number=10.0))
        
    def test_round_half_up(self):
        with Utils() as utils:
            self.assertRaises(
                TypeError, utils.round_half_up)
            self.assertRaises(
                TypeError, utils.round_half_up, number=None, decimals=0)
            self.assertRaises(
                TypeError, utils.round_half_up, number=42.65, decimals='None')
            self.assertRaises(
                TypeError, utils.round_half_up, number=42.65, decimals=43.2)
            self.assertEquals(11, utils.round_half_up(number=11.1))
            self.assertEquals(11, utils.round_half_up(number=11.0))
            self.assertEquals(10, utils.round_half_up(number=10.4))
            self.assertEquals(11, utils.round_half_up(number=10.5))
            self.assertEquals(10, utils.round_half_up(number=10.0))
    
    def test_round_half_down(self):
        with Utils() as utils:
            self.assertRaises(
                TypeError, utils.round_half_down)
            self.assertRaises(
                TypeError, utils.round_half_down, number=None, decimals=0)
            self.assertRaises(
                TypeError, utils.round_half_down, number=42.65, decimals='None')
            self.assertRaises(
                TypeError, utils.round_half_down, number=42.65, decimals=43.2)
            self.assertEquals(11, utils.round_half_down(number=11.1))
            self.assertEquals(11, utils.round_half_down(number=11.0))
            self.assertEquals(10, utils.round_half_down(number=10.4))
            self.assertEquals(10, utils.round_half_down(number=10.5))
            self.assertEquals(10, utils.round_half_down(number=10.0))

    def test_percent_to_linear(self):
        
        with Utils() as utils:
            list_t = [
                16,
                256,
                2048,
                4096,
                65536,
                1048576,
                16777216,
                4294967296,
                281474976710656,
                18446744073709551616,
            ]

            for i in list_t:
                self.assertEqual(
                    i, utils.percent_to_linear(percent=0, maximum=i)
                )
                self.assertEqual(
                    i / 2, utils.percent_to_linear(percent=50, maximum=i)
                )
                self.assertEqual(
                    0, utils.percent_to_linear(percent=100, maximum=i)
                )
            self.assertRaises(
                TypeError, utils.percent_to_linear, percent=42, maximum="42"
            )
            self.assertRaises(
                TypeError, utils.percent_to_linear, percent="42", maximum=42
            )

    def test_linear_to_percent(self):
        with Utils() as utils:
            list_t = [
                16,
                256,
                2048,
                4096,
                65536,
                1048576,
                16777216,
                4294967296,
                281474976710656,
                18446744073709551616,
            ]
            for i in list_t:
                self.assertEqual(0, utils.linear_to_percent(value=0, maximum=i))
                self.assertEqual(
                    50, utils.linear_to_percent(value=i / 2, maximum=i)
                )
                self.assertEqual(
                    100, utils.linear_to_percent(value=i, maximum=i)
                )
            self.assertRaises(
                TypeError, utils.linear_to_percent, percent=42, maximum="42"
            )
            self.assertRaises(
                TypeError, utils.linear_to_percent, percent="42", maximum=42
            )

    def test_linear_to_db(self):
        # Inspired by: https://en.wikipedia.org/wiki/Audio_bit_depth
        with Utils() as utils:
            self.assertTrue(isneginf(utils.linear_to_db(0)))

            self.assertEqual(24.082399653118497, utils.linear_to_db(16))
            # 8 bits
            self.assertEqual(48.164799306236993, utils.linear_to_db(256))
            # 11 bits
            self.assertEqual(66.226599046075862, utils.linear_to_db(2048))
            # 12 bits
            self.assertEqual(72.247198959355487, utils.linear_to_db(4096))
            # 16 bits
            self.assertEqual(96.329598612473987, utils.linear_to_db(65536))
            # 20 bits
            self.assertEqual(120.41199826559249, utils.linear_to_db(1048576))
            # 24 bits
            self.assertEqual(144.49439791871097, utils.linear_to_db(16777216))
            # 32 bits
            self.assertEqual(192.65919722494797, utils.linear_to_db(4294967296))
            # 48 bits
            self.assertEqual(
                288.98879583742195, utils.linear_to_db(281474976710656)
            )
            # 64 bits
            self.assertRaises(
                TypeError, utils.linear_to_db, 18446744073709551616
            )

            self.assertRaises(TypeError, utils.linear_to_db, factor=None)

    def test__db_to_linear(self):
        with Utils() as utils:
            # 4 bits
            self.assertEqual(utils.db_to_linear(24.082399653118497), 16)
            # 8 bits
            self.assertEqual(utils.db_to_linear(48.164799306236993), 256)
            # 11 bits
            self.assertEqual(utils.db_to_linear(66.226599046075862), 2048)
            # 12 bits
            self.assertEqual(utils.db_to_linear(72.247198959355487), 4096)
            # 16 bits
            self.assertEqual(utils.db_to_linear(96.329598612473987), 65536)
            # 20 bits
            self.assertEqual(utils.db_to_linear(120.41199826559249), 1048576)
            # 24 bits
            self.assertEqual(utils.db_to_linear(144.49439791871097), 16777216)
            # 32 bits
            self.assertEqual(utils.db_to_linear(192.65919722494797), 4294967296)
            # 48 bits
            self.assertEqual(
                utils.db_to_linear(288.98879583742195), 281474976710656
            )
            # 64 bits
            # self.assertRaises(TypeError, utils.db_to_linear,
            #                   18446744073709551616)

            self.assertRaises(TypeError, utils.db_to_linear, gain=None)

    def test_to_percent(self):
        with Utils() as utils:
            self.assertRaises(
                TypeError, utils.to_percent, value="42", maximum=75
            )
            self.assertRaises(
                TypeError, utils.to_percent, value=42, maximum="75"
            )
            self.assertRaises(
                ZeroDivisionError, utils.to_percent, value=42, maximum=0
            )

            self.assertTrue(
                isinstance(utils.to_percent(value=42.0, maximum=4200), float)
            )

            self.assertTrue(
                isinstance(utils.to_percent(value=42, maximum=4200), int)
            )

            # 4 bits
            self.assertEqual(100, utils.to_percent(16, 16))
            self.assertEqual(50, utils.to_percent(16 / 2, 16))
            self.assertEqual(0, utils.to_percent(0, 16))
            # 8 bits
            self.assertEqual(100, utils.to_percent(256, 256))
            self.assertEqual(50, utils.to_percent(256 / 2, 256))
            self.assertEqual(0, utils.to_percent(0, 256))
            # 11 bits
            self.assertEqual(100, utils.to_percent(2048, 2048))
            self.assertEqual(50, utils.to_percent(2048 / 2, 2048))
            self.assertEqual(0, utils.to_percent(0, 2048))
            # 12 bits
            self.assertEqual(100, utils.to_percent(4096, 4096))
            self.assertEqual(50, utils.to_percent(4096 / 2, 4096))
            self.assertEqual(0, utils.to_percent(0, 4096))
            # 16 bits
            self.assertEqual(100, utils.to_percent(65536, 65536))
            self.assertEqual(50, utils.to_percent(65536 / 2, 65536))
            self.assertEqual(0, utils.to_percent(0, 65536))
            # 20 bits
            self.assertEqual(100, utils.to_percent(1048576, 1048576))
            self.assertEqual(50, utils.to_percent(1048576 / 2, 1048576))
            self.assertEqual(0, utils.to_percent(0, 1048576))
            # 24 bits
            self.assertEqual(100, utils.to_percent(16777216, 16777216))
            self.assertEqual(50, utils.to_percent(16777216 / 2, 16777216))
            self.assertEqual(0, utils.to_percent(0, 16777216))
            # 32 bits
            self.assertEqual(100, utils.to_percent(4294967296, 4294967296))
            self.assertEqual(50, utils.to_percent(4294967296 / 2, 4294967296))
            self.assertEqual(0, utils.to_percent(0, 4294967296))
            # 48 bits
            self.assertEqual(
                100, utils.to_percent(281474976710656, 281474976710656)
            )
            self.assertEqual(
                50, utils.to_percent(281474976710656 / 2, 281474976710656)
            )
            self.assertEqual(0, utils.to_percent(0, 281474976710656))
            # 64 bits
            # self.assertEqual(
            #     100,
            #     utils.to_percent(18446744073709551616, 18446744073709551616))

            # self.assertEqual(
            #     50,
            #     utils.to_percent(
            #           18446744073709551616 / 2, 18446744073709551616))
            # self.assertEqual(
            #     0,
            #     utils.to_percent(0, 18446744073709551616))

    def test_from_percent(self):
        with Utils() as utils:
            self.assertRaises(
                TypeError, utils.from_percent, value="42", maximum=75
            )
            self.assertRaises(
                TypeError, utils.from_percent, value=42, maximum="75"
            )

            self.assertTrue(
                isinstance(utils.from_percent(value=42.0, maximum=4200), float)
            )

            # Should work on a 64bits system
            # list_t = [
            #     16,
            #     256,
            #     2048,
            #     4096,
            #     65536,
            #     1048576,
            #     16777216,
            #     4294967296,
            #     281474976710656,
            #     18446744073709551616
            # ]

            # Should work on a 32bits system
            list_t = [
                16,
                256,
                2048,
                4096,
                65536,
                1048576,
                16777216,
                4294967296,
                281474976710656,
            ]
            for i in list_t:
                self.assertEqual(0, utils.from_percent(0, i))
                self.assertEqual(i / 2, utils.from_percent(50, i))
                self.assertEqual(0, utils.from_percent(0, i))

    def test_clamp(self):
        with Utils() as utils:
            self.assertEqual(42, utils.clamp(value=1, smallest=42, largest=100))
            self.assertEqual(
                42.0, utils.clamp(value=1.0, smallest=42, largest=100)
            )
            self.assertEqual(
                1.0, utils.clamp(value=1.0, smallest=-42, largest=100)
            )

            self.assertEqual(42, utils.clamp(value=42, smallest=42, largest=42))
            self.assertEqual(10, utils.clamp(value=42, smallest=0, largest=10))
            self.assertEqual(
                42.0, utils.clamp(value=100.0, smallest=0, largest=42)
            )
            self.assertEqual(
                42.0, utils.clamp(value=42.0, smallest=0, largest=420)
            )

            self.assertRaises(
                TypeError, utils.clamp, value=str(""), smallest=0, largest=42
            )
            self.assertRaises(
                TypeError, utils.clamp, value=100, smallest=str(""), largest=42
            )
            self.assertRaises(
                TypeError, utils.clamp, value=100, smallest=0, largest=str("")
            )

    def test_sec2time(self):
        with Utils() as utils:
            self.assertEqual(utils.sec2time(10, 3), "00:00:10.000")
            self.assertEqual(
                utils.sec2time(1234567.8910, 0), "14 days, 06:56:07"
            )
            self.assertEqual(
                utils.sec2time(1234567.8910, 4), "14 days, 06:56:07.8910"
            )
            self.assertEqual(
                utils.sec2time([12, 345678.9], 3),
                ["00:00:12.000", "4 days, 00:01:18.900"],
            )

    def test_sizeof(self):
        with Utils() as utils:
            # Ref: https://en.wikipedia.org/wiki/Metric_prefix
            self.assertEqual("1Y", utils.sizeof(1000000000000000000000000))
            self.assertEqual("1Z", utils.sizeof(1000000000000000000000))
            self.assertEqual("1E", utils.sizeof(1000000000000000000))
            self.assertEqual("1P", utils.sizeof(1000000000000000))
            self.assertEqual("1T", utils.sizeof(1000000000000))
            self.assertEqual("1G", utils.sizeof(1000000000))
            self.assertEqual("1M", utils.sizeof(1000000))
            self.assertEqual("1K", utils.sizeof(1000))
            self.assertEqual("10", utils.sizeof(10))

            self.assertRaises(TypeError, utils.sizeof, "42")

    def test_utils_disk_usage(self):
        with Utils() as utils:
            self.assertEqual(str, type(utils.disk_usage(".")))
            self.assertGreater(len(utils.disk_usage(".")), 7)

            self.assertRaises(TypeError, utils.disk_usage, path=42)
            self.assertRaises(ValueError, utils.disk_usage, path="42")


if __name__ == "__main__":
    unittest.main()
