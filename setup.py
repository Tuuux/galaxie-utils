from setuptools import setup

setup(
    name='galaxie-utils',
    version='0.1',
    packages=['GLXUtils'],
    url='https://gitlab.com/Tuuux/galaxie-utils',
    license='GPL3',
    author='tuxa',
    author_email='tuxa@rtnp.org',
    description='utils for everything',
    tests_require=['green']
)
