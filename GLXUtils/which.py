#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html

import os


def which(program_name=None, all=False):
    """
    Simulates unix `which` command by returns absolute path if program_name \
        is found or raise a EnvironmentError.

    :param program_name: The program_name name you searching for
    :type program_name: :py:data:`string`
    :param all: return a list with  all matching executables in PATH, not \
        just the first.
    :type all: bool
    :return: program_name real path
    :rtype: :py:data:`string` or :py:data:`list`
    :raise TypeError: when "program_name" argument is not a :py:data:`string`
    :raise EnvironmentError: when "program_name" argument is not found on $PATH`
    """
    
    if type(program_name) != str:
        raise TypeError("'program_name' must be a str")

    if not all:
        for path in os.environ["PATH"].split(os.pathsep):
            if os.path.isfile(os.path.join(path, program_name)):
                if os.access(os.path.join(path, program_name), os.X_OK):
                    return os.path.join(path, program_name)
    else:
        return_list = []
        for path in os.environ["PATH"].split(os.pathsep):
            if os.path.isfile(os.path.join(path, program_name)):
                if os.access(os.path.join(path, program_name), os.X_OK):
                    return_list.append(os.path.join(path, program_name))

        if return_list:
            return return_list

    raise EnvironmentError("{0} is not found on $PATH".format(program_name))
