#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This script is published under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Utils Team, all rights reserved

import math
import os

# from numpy import array_equal
# from numpy import polyfit, sqrt, mean, absolute, log10, arange, power
from numpy import log10
import six


class Utils(object):
    def __init__(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
    
    @staticmethod
    def round_up(number=None, decimals=0):
        """
        Round a number to up:
        
        example: 10.1 will be round to 11

        Inspired by: https://realpython.com/python-rounding/

        :param number: the number to round
        :type number: float
        :param decimals: number of decimal
        :type decimals: int
        :return: return int if decimal=0 or float
        :rtype: int or float
        :raise TypeError: when number is not a float
        :raise TypeError: when decimal is not a int
        """
        if type(number) != float:
            raise TypeError('"number" must be float type')

        if type(decimals) != int:
            raise TypeError("'decimals' must be int")

        return math.ceil(number * 10 ** decimals) / 10 ** decimals
    
    @staticmethod
    def round_down(number=None, decimals=0):
        """
        https://realpython.com/python-rounding/

        :param number: the number to round
        :type number: float
        :param decimals: number of decimal
        :type decimals: int
        :return: return int if decimal=0 or float
        :rtype: int or float
        :raise TypeError: when number is not a float
        :raise TypeError: when decimal is not a int
        """
        if type(number) != float:
            raise TypeError("'number' must be float")

        if type(decimals) != int:
            raise TypeError("'decimals' must be int")

        return math.floor(number * 10 ** decimals) / 10 ** decimals

    @staticmethod
    def round_half_up (number=None, decimals=0):
        """
        https://realpython.com/python-rounding/

        :param number: the number to round
        :type number: float
        :param decimals: number of decimal
        :type decimals: int
        :return: return int if decimal=0 or float
        :rtype: int or float
        :raise TypeError: when number is not a float
        :raise TypeError: when decimal is not a int
        """
        if type(number) != float:
            raise TypeError("'number' must be float")

        if type(decimals) != int:
            raise TypeError("'decimals' must be int")

        return math.floor(number * 10 ** decimals + 0.5) / 10 ** decimals

    @staticmethod
    def round_half_down(number=None, decimals=0):
        """
        https://realpython.com/python-rounding/

        :param number: the number to round
        :type number: float
        :param decimals: number of decimal
        :type decimals: int
        :return: return int if decimal=0 or float
        :rtype: int or float
        :raise TypeError: when number is not a float
        :raise TypeError: when decimal is not a int
        """
        if type(number) != float:
            raise TypeError("'number' must be float")

        if type(decimals) != int:
            raise TypeError("'decimals' must be int")

        multiplier = 10 ** decimals
        return math.ceil(number * multiplier - 0.5) / multiplier

    @staticmethod
    def linear_to_db(factor=None):
        """
        Return the level of a field quantity in decibels.

        linear_to_db(x) = log(factor) * 20.0

        :param factor:
        :type factor: int or float
        :return: log(x) * 20.0
        :rtype: float
        """
        if not isinstance(factor, int):
            raise TypeError("num must be a int type")

        return log10(factor) * 20.0

    @staticmethod
    def db_to_linear(gain=None):
        """
        Return the level of a field quantity sample.

        db-to-linear(x) = 10^(gain / 20.0)

        :return: 10^(gain / 20.0)
        :rtype : float
        """
        if not isinstance(gain, int) and not isinstance(gain, float):
            raise TypeError("num must be a int or a float type")

        return round(10 ** (gain / 20.0))

    @staticmethod
    def percent_to_linear(percent=None, maximum=None):
        """
        compute a value from percentage

        :param percent: a value
        :type percent: int or float
        :param maximum:
        :type maximum: int or float
        :return: maximum - (maximum * (percent / 100))
        :rtype: int or float
        """
        if not isinstance(percent, int) and not isinstance(percent, float):
            raise TypeError('"percent" must be a int or float type')

        if not isinstance(maximum, int) and not isinstance(maximum, float):
            raise TypeError('"maximum" must be a int or float type')

        return maximum - (maximum * (percent / 100))

    def linear_to_percent(self, value, maximum):
        """compute a percentage from value"""
        return value / maximum * 100

    @staticmethod
    def clamp(value=None, smallest=None, largest=None):
        """
        Back ``value`` inside ``smallest`` and ``largest`` value range.

        :param value: The value it have to be clamped
        :param smallest: The lower value
        :param largest: The upper value
        :type value: int or float
        :type value: int or float
        :return: The clamped value it depend of parameters value type, int or \
        float will be preserve.
        :rtype: int or float
        """
        if not isinstance(value, int) and not isinstance(value, float):
            raise TypeError('"value" must be a int or float type')

        if not isinstance(smallest, int) and not isinstance(smallest, float):
            raise TypeError('"smallest" must be a int or float type')

        if not isinstance(largest, int) and not isinstance(largest, float):
            raise TypeError('"largest" must be a int or float type')

        if isinstance(value, int):
            if value < smallest:
                return int(smallest)

            elif value > largest:
                return int(largest)

            else:
                return int(value)

        elif isinstance(value, float):
            if value < smallest:
                return float(smallest)

            elif value > largest:
                return float(largest)

            else:
                return float(value)

    def sec2time(self, sec, n_msec=3):
        """
        Convert seconds to 'D days, HH:MM:SS.FFF'

        By: Lee
        Source: \
        https://stackoverflow.com/questions/775049/how-do-i-convert-seconds-to-hours-minutes-and-seconds

        Example:
        $ sec2time(10, 3)
        Out: '00:00:10.000'

        $ sec2time(1234567.8910, 0)
        Out: '14 days, 06:56:07'

        $ sec2time(1234567.8910, 4)
        Out: '14 days, 06:56:07.8910'

        $ sec2time([12, 345678.9], 3)
        Out: ['00:00:12.000', '4 days, 00:01:18.900']

        :param sec: time in second
        :type sec: int
        :param n_msec:
        :return:
        """
        if hasattr(sec, "__len__"):
            return [self.sec2time(s) for s in sec]
        m, s = divmod(sec, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        if n_msec > 0:
            pattern = "%%02d:%%02d:%%0%d.%df" % (n_msec + 3, n_msec)
        else:
            pattern = r"%02d:%02d:%02d"
        if d == 0:
            return pattern % (h, m, s)
        return ("%d days, " + pattern) % (d, h, m, s)

    @staticmethod
    def sizeof(num=None):
        """
        return a human readable size like 1K 1Mo

        :param num: the bit size to convert in humain readble thing
        :type: int
        :return: a readable size
        :rtype: str
        :raise TypeError: if num is not a int
        """
        # if type(num) != int:
        #     raise TypeError("num must be a int type")

        i = 0 if num < 1 else int(math.log(num, 1024)) + 1
        v = num / math.pow(1024, i)
        v, i = (v, i) if v > 0.5 else (v * 1024, (i - 1 if i else 0))

        return str(
            int(round(v, 0))) + ["", "K", "M", "G", "T", "P", "E", "Z", "Y"][i]
        

    def disk_usage(self, path="."):
        """
        Return something like: 94G/458G (20%).

        Where:
         - 94G  will be the disk free space
         - 458G will be the disk total space
         - (20%) is the disk free space in percent

        The function request about the drive space where is store the file or \
        directory pass in ``path`` argument.

        The default value is '.', for return disk usage information's about \
        the disk where is store the script file.

        :param path: a existing file path, the disk where is store it file \
        will be use for disk usage.

        :type path: str
        :return: something like 94G/458G (20%) with '.' as ``path`` argument
        :rtype: str
        :raise TypeError: if path is not a str type
        :raise ValueError: if the path don't exist
        """
        six.ensure_str(path)

        if not os.path.exists(os.path.abspath(path)):
            raise ValueError("'path' must be a existing file path")

        st = os.statvfs(os.path.abspath(path))

        return "{0}/{1} ({2}%)".format(
            self.sizeof((st.f_bsize * st.f_bavail)),
            self.sizeof((st.f_blocks * st.f_frsize)),
            str(
                int(
                    self.to_percent(
                        value=float((st.f_bsize * st.f_bavail)),
                        maximum=(st.f_blocks * st.f_frsize),
                    )
                )
            ),
        )

    @staticmethod
    def from_percent(value=None, maximum=None):
        """
        compute a value from percentage

        :param value: the value
        :type value: int or float
        :param maximum: the maximum value
        :type maximum: int or float
        :return: maximum * value / 100
        :rtype: float
        :raise TypeError: when value is not a int or a float type
        :raise TypeError: when maximum is not a int or a float type
        """
        if type(value) != int and type(value) != float:
            raise TypeError("'value' must be a int or a float type")

        if type(maximum) != int and type(maximum) != float:
            raise TypeError("'maximum' must be a int or a float type")

        return maximum * value / 100

    @staticmethod
    def to_percent(value=None, maximum=None):
        """
        compute a percentage

        :param value: a value to compute
        :type value: int or float
        :param maximum:
        :type maximum: int or float
        :return:
        :rtype: int or float
        :raise TypeError: when ``value`` is not a int or a float type
        :raise TypeError: when ``maximum`` is not a int or a float type
        """
        if not isinstance(value, int) and not isinstance(value, float):
            raise TypeError('"value" must be a int or float type')

        if not isinstance(maximum, int) and not isinstance(maximum, float):
            raise TypeError('"value" must be a int or float type')

        if isinstance(value, int):
            return int(value * 100 / maximum)

        elif isinstance(value, float):
            return float(value * 100 / maximum)
